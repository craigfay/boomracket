let players = [
  'Jim',
  'Dave',
  'Alexis',
  'Antonio',
  'Alex',
  'Jenny',
  'Micky',
  'Corey',
  'Henry',
]

let size = players.length - 1;

// Build Bracket
let bracket = Array.from({length: size}, x => x).map((x, i) => {
  let log2 = Math.ceil(Math.log2(i + 1));
  return {
    round: size / 2 - log2,
    participants: []
  }
});

// Fill Bracket
for (let i = bracket.length-1; i > 0; i--) {
  bracket[i].participants.push(players.pop());
  bracket[i].participants.push(players.pop());
}