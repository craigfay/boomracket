let players = [
  'Jim',
  'Dave',
  'Alexis',
  'Alex',
  'Jenny',
  'Antonio',
  // 'Micky',
  // 'Corey',
  // 'Henry',
  // 'Bobby',
]

let size = players.length - 1;

// Build Bracket
let bracket = Array.from({length: size}, x => x).map((x, i) => {
  let log2 = Math.ceil(Math.log2(i + 2));
  return {
    round: Math.ceil(size / 2) - log2,
    participants: []
  }
});

// Fill Bracket
let floor = Math.pow(2, Math.floor(Math.log2(size)));
for (let i = bracket.length-1; i >= 0; i--) {
  if (i + 2 > floor) {
    bracket[i].participants.push(players.pop());
    bracket[i].participants.push(undefined);
  } else {
    bracket[i].participants.push(players.pop());
    bracket[i].participants.push(players.pop());
  }
}
 
bracket = bracket.reverse();
console.log(bracket);